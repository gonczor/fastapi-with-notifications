from fastapi.testclient import TestClient
from pytest import fixture
from kink import di

from src.connectors.database import BaseOrderDatabaseConnector, DummyOrderDatabaseConnector, dummy_orders
from src.notifications.subscribers.backends import BaseBackend, AsyncioBackend
from src.main import app


@fixture(autouse=True)
def swap_db_dependency():
    # Reset dummy database
    global dummy_orders 
    dummy_orders = [{"id": 1, "value": 123, "name": "Brave New World", "status": "new"}]
    di[BaseOrderDatabaseConnector] = DummyOrderDatabaseConnector()


@fixture(autouse=True)
def swap_subscriber_backend_dependency():
    di[BaseBackend] = AsyncioBackend()


@fixture
def client():
    return TestClient(app)
