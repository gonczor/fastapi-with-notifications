from unittest.mock import AsyncMock

import pytest
from fastapi.testclient import TestClient
from kink import di

from src.connectors.database import BaseOrderDatabaseConnector
from src.connectors.delivery import DeliveryConnector
from src.connectors.warehouse import WareHouseConnector
from src.schemas.order import OrderSchemaIn


def test_list(client: TestClient):
    response = client.get("/api/v1/orders/")
    assert len(response.json()) == 1


def test_get(client: TestClient):
    response = client.get("/api/v1/orders/1/")
    assert response.status_code == 200
    assert response.json() == {"id": 1, "value": 123, "name": "Brave New World", "status": "new"}

@pytest.mark.asyncio
async def test_delete(client: TestClient):
    order_data = OrderSchemaIn(value=1, name="some@one.com")
    connector = di[BaseOrderDatabaseConnector]
    order = await connector.create_order(order_data)
    response = client.delete(f"/api/v1/orders/{order['id']}")
    assert response.status_code == 204
    assert len(await connector.list_all()) == 1

@pytest.mark.asyncio
async def test_create(client: TestClient):
    delivery_connector_mock = AsyncMock()
    warehouse_connector_mock = AsyncMock()
    di[DeliveryConnector] = delivery_connector_mock
    di[WareHouseConnector] = warehouse_connector_mock
    db_connector = di[BaseOrderDatabaseConnector]

    response = client.post("/api/v1/orders/", json={"value": 1, "name": "new_name"})

    assert response.status_code == 200
    delivery_connector_mock.send_ready_update.assert_called()
    warehouse_connector_mock.send_ready_update.assert_called()
    orders = await db_connector.list_all()
    assert len(orders) == 2
    assert orders[-1] == {"id": 2, "value": 1, "name": "new_name", "status": "sent"}
