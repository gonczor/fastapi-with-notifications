from fastapi import APIRouter
from kink import di

from src.schemas.order import OrderSchemaIn, OrderSchemaOut
from src.services.order_service import OrderService

router = APIRouter(prefix="/api/v1/orders")


@router.get("/", response_model=list[OrderSchemaOut])
async def list_orders():
    service = di[OrderService]
    return await service.list_orders()


@router.get("/{order_id}", response_model=OrderSchemaOut)
async def get_by_id(order_id: int):
    service = di[OrderService]
    return await service.get_order(order_id)


@router.post("/", response_model=OrderSchemaOut)
async def create_order(order: OrderSchemaIn):
    service = di[OrderService]
    return await service.create_order(order)


@router.delete("/{order_id}", status_code=204)
async def delete_order(order_id: int):
    service: OrderService = di[OrderService]
    await service.delete_order(order_id)
