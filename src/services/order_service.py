from kink import inject

from src.connectors.database import BaseOrderDatabaseConnector
from src.connectors.delivery import DeliveryConnector
from src.connectors.email import EmailConnector
from src.connectors.warehouse import WareHouseConnector
from src.notifications.messages.order import OrderCreatedMessage, OrderPackedMessage
from src.notifications.subjects.order_subjects import (
    OrderCreatedSubject,
    OrderPreparedSubject,
)
from src.schemas.order import OrderSchemaIn, OrderSchemaOut


@inject
class OrderService:
    def __init__(
        self,
        database_connector: BaseOrderDatabaseConnector,
        order_create_subject: OrderCreatedSubject,
    ):
        self.__database_connector = database_connector
        self.__order_create_subject = order_create_subject

    async def create_order(self, order_data: OrderSchemaIn):
        order = await self.__database_connector.create_order(order_data)
        self.__order_create_subject.notify(OrderCreatedMessage(order["id"]))
        return order

    async def list_orders(self) -> list[OrderSchemaOut]:
        return await self.__database_connector.list_all()

    async def get_order(self, order_id: int) -> OrderSchemaOut:
        return await self.__database_connector.get_by_id(order_id)

    async def delete_order(self, order_id: int):
        await self.__database_connector.delete_by_id(order_id)


@inject
class OrderPreparationService:
    def __init__(
        self,
        warehouse_connector: WareHouseConnector,
        database_connector: BaseOrderDatabaseConnector,
        order_prepared_subject: OrderPreparedSubject,
    ):
        self.__database_connector = database_connector
        self.__warehouse_connector = warehouse_connector
        self.__order_prepared_subject = order_prepared_subject

    async def prepare_order(self, order_id: int):
        print("Preparing order")
        await self.__warehouse_connector.send_ready_update(order_id=order_id)
        await self.__database_connector.update_status(order_id, "prepared")
        self.__order_prepared_subject.notify(OrderPackedMessage(order_id=order_id))


@inject
class OrderDeliveryService:
    def __init__(
        self,
        delivery_connector: DeliveryConnector,
        database_connector: BaseOrderDatabaseConnector,
    ):
        self.__database_connector = database_connector
        self.__delivery_connector = delivery_connector

    async def deliver_order(self, order_id: int):
        print("Sending order")
        await self.__delivery_connector.send_ready_update(order_id=order_id)
        await self.__database_connector.update_status(order_id, "sent")


@inject
class EmailNotificationService:
    def __init__(self, email_connector: EmailConnector, db_connector: BaseOrderDatabaseConnector):
        self.__email_connector = email_connector
        self.__db_connector = db_connector

    async def notify(self, order_id: int):
        print("Beginning sending the email")
        order = await self.__db_connector.get_by_id(order_id)
        self.__email_connector.send_email(
            recipient=order["name"], body=f"Your order with id {order['id']} is being sent."
        )