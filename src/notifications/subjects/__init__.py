from abc import ABC

from src.notifications.messages import Message
from src.notifications.subscribers import Subscriber


class Subject(ABC):
    _subscribers: list[Subscriber]

    def attach(self, subscriber: Subscriber):
        self._subscribers.append(subscriber)

    def notify(self, message: Message):
        for subscriber in self._subscribers:
            subscriber.receive(message)
