from src.notifications.messages.order import OrderCreatedMessage, OrderPackedMessage

from . import Subject


class OrderCreatedSubject(Subject):
    _subscribers = []

    def notify(self, message: OrderCreatedMessage):
        super().notify(message=message)


class OrderPreparedSubject(Subject):
    _subscribers = []

    def notify(self, message: OrderPackedMessage):
        super().notify(message=message)
