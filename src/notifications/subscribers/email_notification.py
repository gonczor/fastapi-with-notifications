from kink import inject

from . import Subscriber
from .backends import BaseBackend
from src.notifications.messages.order import OrderPackedMessage
from src.tasks import notify_about_order_sent


@inject
class EmailNotificationSubscriber(Subscriber):
    def __init__(self, backend: BaseBackend):
        super().__init__(backend=backend)

    def receive(self, message: OrderPackedMessage):
        print("Beginning sending the email")
        self._backend.handle(notify_about_order_sent, order_id=message.order_id)
