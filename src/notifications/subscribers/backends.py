from abc import ABC, abstractmethod
from asyncio import get_running_loop
from typing import Callable


class BaseBackend(ABC):
    @abstractmethod
    def handle(self, handler: Callable, **kwargs):
        pass


class AsyncioBackend(BaseBackend):
    def handle(self, handler: Callable, **kwargs):
        handler(**kwargs)


class CeleryBackend(BaseBackend):
    def handle(self, handler: Callable, **kwargs):
        handler.apply_async(kwargs=kwargs)
