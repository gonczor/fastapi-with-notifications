from abc import ABC, abstractmethod

from kink import inject

from src.notifications.subscribers.backends import BaseBackend
from src.notifications.messages import Message



@inject
class Subscriber(ABC):
    def __init__(self, backend: BaseBackend):
        self._backend = backend

    @abstractmethod
    def receive(self, message: Message):
        pass
