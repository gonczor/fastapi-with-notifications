from kink import inject

from src.notifications.subscribers.backends import BaseBackend
from src.notifications.messages.order import OrderCreatedMessage
from src.services.order_service import OrderPreparationService
from src.tasks import order_preparation_task

from . import Subscriber


@inject
class WareHouseSubscriber(Subscriber):
    def __init__(self, backend: BaseBackend):
        super().__init__(backend=backend)

    def receive(self, message: OrderCreatedMessage):
        print(f"Wrehouse team received message {message}")
        self._backend.handle(order_preparation_task, order_id=message.order_id)
