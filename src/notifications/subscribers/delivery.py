from kink import inject

from src.notifications.subscribers.backends import BaseBackend
from src.notifications.messages.order import OrderPackedMessage
from src.services.order_service import OrderDeliveryService
from src.tasks import deliver_order_task
from . import Subscriber


@inject
class DeliverySubscriber(Subscriber):
    def __init__(self, backend: BaseBackend):
        super().__init__(backend=backend)

    def receive(self, message: OrderPackedMessage):
        print(f"Delivery team received message {message}")
        self._backend.handle(deliver_order_task, order_id=message.order_id)
