from . import Message


class OrderCreatedMessage(Message):
    __order_id: int

    def __init__(self, order_id: int):
        self.__order_id = order_id
        super().__init__()

    @property
    def order_id(self) -> int:
        return self.__order_id

    def as_json(self) -> dict:
        return {"order_id": self.__order_id}


class OrderPackedMessage(Message):
    __order_id: int

    def __init__(self, order_id: int):
        self.__order_id = order_id
        super().__init__()

    @property
    def order_id(self) -> int:
        return self.__order_id

    def as_json(self) -> dict:
        return {"order_id": self.__order_id}
