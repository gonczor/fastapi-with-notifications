from abc import ABC, abstractmethod


class Message(ABC):
    @abstractmethod
    def as_json(self) -> dict:
        pass
