from celery import Celery

from src.config import get_settings


settings = get_settings()


app = Celery("sub-prod", broker=settings.celery_broker_url)
app.autodiscover_tasks()
