from fastapi import FastAPI, Request
from fastapi.responses import JSONResponse

from src.views.orders import router as orders_router
from src.connectors.database import NotFoundError

app = FastAPI()
app.include_router(orders_router)


@app.exception_handler(NotFoundError)
async def not_found_error(request: Request, exc: NotFoundError):
    return JSONResponse(status_code=404, content={"message": exc.message})


if __name__ == "__main__":
    import uvicorn

    uvicorn.run("main:app", host="127.0.0.1", port=5000, reload=True)
