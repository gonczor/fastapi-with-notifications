from functools import lru_cache

from pydantic import BaseSettings


class Config(BaseSettings):
    celery_broker_url: str = "amqp://localhost:5672/"
    database_uri: str = "main.db"
    use_celery_backend: bool = False


@lru_cache
def get_settings() -> Config:
    return Config()
