from abc import ABC, abstractmethod

from aiosqlite3 import connect

from src.config import get_settings
from src.schemas.order import OrderSchemaIn

settings = get_settings()
dummy_orders = [{"id": 1, "value": 123, "name": "Brave New World", "status": "new"}]


class NotFoundError(Exception):
    message: str

    def __init__(self, message):
        self.message = message


class BaseOrderDatabaseConnector(ABC):
    @abstractmethod
    async def create_order(self, order_data: OrderSchemaIn) -> dict:
        pass

    @abstractmethod
    async def list_all(self) -> list[dict]:
        pass

    @abstractmethod
    async def get_by_id(self, order_id: int):
        pass

    @abstractmethod
    async def delete_by_id(self, order_id: int):
        pass

    @abstractmethod
    async def update_status(self, order_id: int, status: str):
        pass


class DummyOrderDatabaseConnector(BaseOrderDatabaseConnector):
    async def create_order(self, order_data: OrderSchemaIn) -> dict:
        order = {"id": len(dummy_orders) + 1, "status": "new"} | order_data.dict()
        dummy_orders.append(order)
        return order

    async def list_all(self) -> list[dict]:
        return dummy_orders

    async def get_by_id(self, order_id: int):
        for order in dummy_orders:
            if order_id == order["id"]:
                return order
        raise NotFoundError(f"Order with id {order_id} not found.")

    async def delete_by_id(self, order_id: int):
        for index, order in enumerate(dummy_orders):
            if order_id == order["id"]:
                dummy_orders.pop(index)
                return
        raise NotFoundError(f"Order with id {order_id} not found.")

    async def update_status(self, order_id: int, status: str):
        for order in dummy_orders:
            if order_id == order["id"]:
                order["status"] = status
                return
        raise NotFoundError(f"Order with id {order_id} not found.")


class DatabaseConnector(BaseOrderDatabaseConnector):
    __uri = settings.database_uri

    async def create_order(self, order_data: OrderSchemaIn) -> dict:
        async with connect(self.__uri) as db:
            result = await db.execute(
                "INSERT INTO orders (value, name) VALUES (?, ?)", (order_data.value, order_data.name)
            )
            await db.commit()
            return await self.get_by_id(result.lastrowid)

    async def list_all(self) -> list[dict]:
        results = []
        async with connect(self.__uri) as db:
            async with db.execute("SELECT id, value, name, status FROM orders") as cursor:
                for row in await cursor.fetchall():
                    results.append(
                        {
                            "id": row[0],
                            "value": row[1],
                            "name": row[2],
                            "status": row[3]
                        }
                    )
        return results
    
    async def delete_by_id(self, order_id: int):
        async with connect(self.__uri) as db:
            await db.execute(
                f"DELETE FROM orders WHERE id={order_id}"
            )
            await db.commit()

    async def get_by_id(self, order_id: int) -> dict:
        async with connect(self.__uri) as db:
            async with db.execute(f"SELECT id, value, name, status FROM orders WHERE id=?", (order_id,)) as cursor:
                row = await cursor.fetchone()
                if row is None:
                    raise NotFoundError(f"Order with id {order_id} not found.")
                return {
                    "id": row[0],
                    "value": row[1],
                    "name": row[2],
                    "status": row[3]
                }


    async def update_status(self, order_id: int, status: str):
        async with connect(self.__uri) as db:
            await db.execute(
                "UPDATE orders SET status=? WHERE id=?", (status, order_id)
            )
            await db.commit()
