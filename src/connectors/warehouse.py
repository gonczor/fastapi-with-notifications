from asyncio import sleep


class WareHouseConnector:
    async def send_ready_update(self, order_id: int):
        await sleep(1)
        print(f"External warehouse system update order {order_id}")
