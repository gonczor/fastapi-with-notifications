from asyncio import sleep


class DeliveryConnector:
    async def send_ready_update(self, order_id: int):
        await sleep(1)
        print(f"Courier picked up the order {order_id}")
