from asyncio import run, get_running_loop

from kink import di

from src.celery_app import app
from src.services.order_service import OrderPreparationService, OrderDeliveryService, EmailNotificationService


@app.task
def order_preparation_task(order_id: int):
    order_preparation_service: OrderPreparationService = di[OrderPreparationService]
    try:
        loop = get_running_loop()
        loop.create_task(order_preparation_service.prepare_order(order_id=order_id))
    except RuntimeError:
        run(order_preparation_service.prepare_order(order_id=order_id))


@app.task
def deliver_order_task(order_id: int):
    service: OrderDeliveryService = di[OrderDeliveryService]
    try:
        loop = get_running_loop()
        loop.create_task(service.deliver_order(order_id=order_id))
    except RuntimeError:
        run(service.deliver_order(order_id=order_id))


@app.task
def notify_about_order_sent(order_id: int):
    service: OrderDeliveryService = di[EmailNotificationService]
    try:
        loop = get_running_loop()
        loop.create_task(service.notify(order_id=order_id))
    except RuntimeError:
        run(service.notify(order_id=order_id))
