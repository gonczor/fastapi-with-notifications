from kink import di

from src.config import get_settings
from src.connectors.database import DatabaseConnector, BaseOrderDatabaseConnector
from src.connectors.delivery import DeliveryConnector
from src.connectors.email import EmailConnector
from src.connectors.warehouse import WareHouseConnector
from src.notifications.subscribers.backends import BaseBackend, AsyncioBackend, CeleryBackend
from src.notifications.subscribers.delivery import DeliverySubscriber
from src.notifications.subscribers.warehouse import WareHouseSubscriber
from src.notifications.subscribers.email_notification import EmailNotificationSubscriber
from src.notifications.subjects.order_subjects import (
    OrderCreatedSubject,
    OrderPreparedSubject,
)
from src.services.order_service import OrderDeliveryService, OrderPreparationService, EmailNotificationService

settings = get_settings()

order_created_subject = OrderCreatedSubject()
order_prepared_subject = OrderPreparedSubject()

# Message backend
di[BaseBackend] = lambda di: CeleryBackend if settings.use_celery_backend else AsyncioBackend()

# Connectors
di[BaseOrderDatabaseConnector] = lambda di: DatabaseConnector()
di[WareHouseConnector] = lambda di: WareHouseConnector()
di[DeliveryConnector] = lambda di: DeliveryConnector()
di[EmailConnector] = lambda di: EmailConnector()

# Services
di[OrderDeliveryService] = lambda di: OrderDeliveryService()
di[OrderPreparationService] = lambda di: OrderPreparationService()
di[EmailNotificationService] = lambda di: EmailNotificationService()

# Subjects
di[OrderCreatedSubject] = order_created_subject
di[OrderPreparedSubject] = order_prepared_subject

# Subscribers
di[WareHouseSubscriber] = lambda di: WareHouseSubscriber()
di[DeliverySubscriber] = lambda di: DeliverySubscriber()
di[EmailNotificationSubscriber] = lambda di: EmailNotificationSubscriber()

# The attachment of subscribers
order_created_subject.attach(di[WareHouseSubscriber])
order_prepared_subject.attach(di[DeliverySubscriber])
order_prepared_subject.attach(di[EmailNotificationSubscriber])
