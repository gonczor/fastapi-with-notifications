from pydantic import BaseModel


class OrderSchemaOut(BaseModel):
    id: int
    value: int
    name: str
    status: str


class OrderSchemaIn(BaseModel):
    value: int
    name: str
